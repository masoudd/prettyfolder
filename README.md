# prettyfolder

A prettier version of go's http.FileServer. With search on directory listings

![Screenshot of prettyfolder serving itself](prettyfolder.png "screenshot")


## Usage

    prettyfolder [-bind <address>] [-port <port>] [-dir <directory>]

- **-bind**: Specify the address for the server to listen on
     - Default is *localhost*
- **-port**: Specify the port for the server to listen on
    - Default is 8080
- **-gzip**: Use gzip
    - gzip is off by default
- **-dir**: Specify the root directory for the server to serve
    - Default is *current directory* (.)
- **-version**: Print version number

see `prettyfolder -help`


## Install
### Compiling
1. Install go: https://golang.org/doc/install
2. Run `go install codeberg.org/masoudd/prettyfolder@latest`. This will install an executable file named `prettyfolder` in your `$GOPATH/bin`. See `go help install` and `go env` for more info.

### Pre built binaries
Download the latest version from [Releases page](https://codeberg.org/masoudd/prettyfolder/releases/)

## Building
 1. Install go: https://golang.org/doc/install
 2. clone the project:
     - `git clone https://codeberg.org/masoudd/prettyfolder`
     - `cd prettyfolder`
 3. `go build` this will create the executable file `prettyfolder` (or `prettyfolder.exe` if you're on windows)
     - Or if you want to build a static executable: `CGO_ENABLED=0 go build`
