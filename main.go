/*
	Copyright (C) 2020-2023  Masoud Naservand

	This file is part of prettyfolder.

	prettyfolder is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	prettyfolder is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with prettyfolder.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime/debug"
	"time"

	"github.com/NYTimes/gziphandler"
)

var logger *log.Logger
var bind string
var port string
var servingDir string
var gzip bool
var version bool

func init() {
	flag.StringVar(&port, "port", "8080", "The port to listen on")
	flag.StringVar(&bind, "bind", "localhost", "The ip to listen on")
	flag.StringVar(&servingDir, "dir", ".", "The directory to serve")
	flag.BoolVar(&gzip, "gzip", false, "Use gzip")
	flag.BoolVar(&version, "version", false, "Print version number")
	logger = log.New(os.Stderr, "prettyfolder: ", log.LstdFlags)
}

func main() {
	flag.Parse()

	if version {
		buildinfo, _ := debug.ReadBuildInfo()
		println("Prettyfolder Version: ", buildinfo.Main.Version)
		println("checksum: ", buildinfo.Main.Sum)
		return
	}

	rootDir := http.Dir(servingDir)
	handler := fileServer(rootDir)
	addr := fmt.Sprintf("%s:%s", bind, port)
	logger.Println("Serving:", rootDir)
	logger.Println("Listening on:", "http://"+addr)
	if gzip {
		handler = gziphandler.GzipHandler(handler)
	}
	srv := http.Server{
		Addr:         addr,
		Handler:      handler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
	logger.Fatal(srv.ListenAndServe())
}
