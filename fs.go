/*
	Copyright (C) 2020-2023  Masoud Naservand

	This file is part of prettyfolder.

	prettyfolder is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	prettyfolder is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with prettyfolder.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	_ "embed"
	"html/template"
	"net/http"
	"path"
	"strings"
	"time"
)

//go:embed static/list.tmpl
var embededString string
var listTemplate *template.Template

type fileHandler struct {
	root http.FileSystem
}

func (f *fileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	upath := r.URL.Path
	if !strings.HasPrefix(upath, "/") {
		upath = "/" + upath
		r.URL.Path = upath
	}

	if strings.HasSuffix(upath, "/index.html") {
		r.URL.Path = ""
	}
	logger.Printf("[%s] %s %s\n", strings.Split(r.RemoteAddr, ":")[0], r.Method, upath)

	// is a directory
	if strings.HasSuffix(upath, "/") {
		listDir(w, r, f.root, upath)
	} else {
		upath = path.Clean(path.Join(servingDir, upath))
		http.ServeFile(w, r, upath)
	}
}

type fileInfo struct {
	Name            string
	Size            int64
	ModTime         time.Time
	ModTimeReadable string
	IsDir           bool
}
type crumb struct {
	Name   string
	Url    string
	IsHere bool
}

type directoryList struct {
	Name        string
	Files       []fileInfo
	Breadcrumbs []crumb
}

// TODO look at this more
func addBreadcrumbs(dirList *directoryList, fullpath string) {
	url := fullpath
	IsHere := true
	name := fullpath
	var bc crumb
	done := false
	for name != "" && !done {
		if url == "/" {
			done = true
			bc = crumb{
				Name:   "Root",
				Url:    url,
				IsHere: IsHere,
			}
		} else {
			dir, n := path.Split(path.Clean(url))
			name = n
			bc = crumb{
				Name:   name,
				Url:    url,
				IsHere: IsHere,
			}
			url = dir
		}
		if IsHere {
			IsHere = false
		}

		//prepend
		dirList.Breadcrumbs = append([]crumb{bc}, dirList.Breadcrumbs...)

	}
}

func listDir(w http.ResponseWriter, r *http.Request, fs http.FileSystem, name string) {
	f, err := fs.Open(name)
	if err != nil {
		logger.Printf("Error: Opening %s failed. %s\n", name, err)
		http.Error(w, ("Could not open directory: " + name), http.StatusInternalServerError)
		return
	}

	dirs, err := f.Readdir(-1)
	if err != nil {
		logger.Printf("Error: Reading directory %s failed. %s\n", name, err)
		http.Error(w, ("Could not get contents of directory: " + name), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	dl := directoryList{Name: name}
	if name != "/" {
		parentPath := path.Dir(path.Clean(name))
		parent, err := fs.Open(parentPath)
		if err != nil {
			logger.Printf("Cannot open directory: %s, Error: %v", parentPath, err)
			http.Error(w, ("Cannot open parent directory"), http.StatusInternalServerError)
			return
		}
		parentStat, err := parent.Stat()
		if err != nil {
			logger.Printf("Cannot stat directory: %s, Error: %v", parentPath, err)
			http.Error(w, ("Cannot stat parent directory"), http.StatusInternalServerError)
			return
		}
		dotdot := fileInfo{
			Name:            "..",
			Size:            parentStat.Size(),
			ModTime:         parentStat.ModTime(),
			ModTimeReadable: parentStat.ModTime().Format("2006-01-02 15:04:05"),
			IsDir:           true,
		}
		dl.Files = append(dl.Files, dotdot)
	}
	for _, dir := range dirs {
		fi := fileInfo{
			Name:            dir.Name(),
			Size:            dir.Size(),
			ModTime:         dir.ModTime(),
			ModTimeReadable: dir.ModTime().Format("2006-01-02 15:04:05"),
			IsDir:           dir.IsDir(),
		}
		if fi.IsDir {
			fi.Name = fi.Name + "/"
		}
		dl.Files = append(dl.Files, fi)
	}
	addBreadcrumbs(&dl, name)
	if listTemplate == nil {
		listTemplate = template.Must(template.New("list.tmpl").Parse(embededString))
	}

	err = listTemplate.Execute(w, dl)
	if err != nil {
		logger.Fatalln("executing template", err)
	}
}

func fileServer(root http.FileSystem) http.Handler {
	return &fileHandler{root}
}
