module codeberg.org/masoudd/prettyfolder

go 1.16

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/stretchr/testify v1.8.0 // indirect
)
